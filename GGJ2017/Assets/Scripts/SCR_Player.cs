﻿using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;
using System.Collections;

public class SCR_Player : MonoBehaviour {

	// Attributes.
	private static bool disableInput = true;
	[SerializeField]	private float movementSpeed = 100.0f;
	[SerializeField]	private float sprintMultiplier = 2.0f;
	[SerializeField]	private float rayDistance = 50.0f;
	private Text startTimer = null;
	private RaycastHit pickUpObject;
	private bool carringObject = false;
	private bool interacting = false;
	private Transform cam = null;
	private Image crosshair = null;
	private float footsteps = 0.0f;
	float horizontal = 0.0f;
	float vertical = 0.0f; 
	float sprint = 0.0f;

	void Start () 
	{
		cam = transform.FindChild("CameraController").FindChild("Main Camera");
		crosshair = GameObject.Find("Crosshair").GetComponent<Image>();
		startTimer = GameObject.Find("StartTimer").GetComponent<Text>();

		StartCoroutine("StartDelay");
	}

	IEnumerator StartDelay()
	{
		yield return new WaitForSeconds(1.0f);
		startTimer.text = "3...";

		yield return new WaitForSeconds(1.0f);
		startTimer.text = "2...";

		yield return new WaitForSeconds(1.0f);
		startTimer.text = "1...";

		yield return new WaitForSeconds(1.0f);
		startTimer.text = "GO!";

		yield return new WaitForSeconds(0.5f);
		startTimer.transform.position = new Vector3(0.0f, 3000.0f, 0.0f);

		disableInput = false;
	}

	void Move()
	{
		if (horizontal != 0.0f || vertical != 0.0f) {
			Vector3 axis = new Vector3 (horizontal * movementSpeed, 0.0f, vertical * movementSpeed);
	
			if (sprint != 0.0f)
				axis *= sprintMultiplier;
			
			axis = cam.TransformDirection (axis);
			GetComponent<Rigidbody> ().velocity = axis;

			footsteps += Time.deltaTime;

			if (footsteps > 0.5f) {
				GetComponent<SCR_Footsteps> ().Play ();
				footsteps = 0.0f;
			}
		}
	}

	void CrosshairUpdate()
	{
		RaycastHit hit;

		if (Physics.Raycast (transform.position, cam.forward, out hit, rayDistance)) 
		{
			if(hit.collider.tag != "Player")
			{
				if(hit.collider.tag == "Pickup")
					crosshair.color = Color.red;
				else
					crosshair.color = Color.white;
			}
		}
		else
			crosshair.color = Color.white;
	}

	public void Drop()
	{
		pickUpObject.collider.GetComponent<Collider>().enabled = true;
		pickUpObject.collider.transform.SetParent (null);
		pickUpObject.collider.GetComponent<Rigidbody>().isKinematic = false;
		carringObject = false;
	}

	public void Pickup(RaycastHit hit)
	{
		pickUpObject = hit;
		pickUpObject.collider.transform.position = cam.FindChild("PickupSpot").position;
		hit.rigidbody.isKinematic = true;
		//pickUpObject.collider.GetComponent<Collider>().enabled = false;
		carringObject = true;
		pickUpObject.collider.transform.SetParent (cam);
	}

	void Interact()
	{
		if (!carringObject) 
		{
			RaycastHit hit;

			if (Physics.Raycast (transform.position, cam.forward, out hit, rayDistance)) 
			{
				if(hit.collider.tag == "Pickup")
					Pickup(hit);
			}
		}
		else 
			Drop();
	}

	// Update is called once per frame
	void Update () 
	{
		if(!disableInput)
		{
			horizontal = CrossPlatformInputManager.GetAxis ("Horizontal");
			vertical = CrossPlatformInputManager.GetAxis ("Vertical");
			sprint = CrossPlatformInputManager.GetAxis("Sprint");

			if (Input.GetMouseButtonDown(0) && !interacting)
				Interact();
		}

		if(!carringObject)
			CrosshairUpdate();
	}

	void FixedUpdate()
	{
		// If we have input.
		if(!disableInput)
			Move();
	}

	public static bool DisableInput
	{
		get { return disableInput; }
		set { disableInput = value; }
	}

	public RaycastHit PickupItem
	{
		get { return pickUpObject; }
		set { pickUpObject = value; }
	}

	public bool Carrying
	{
		get { return carringObject; }
		set { carringObject = value; }
	}

	public bool Interacting
	{
		get { return interacting; }
		set { interacting = value; }
	}
}