﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class SCR_ElevatorDoorTigger : MonoBehaviour {

	[SerializeField]	private AudioClip openClip = null;
	[SerializeField]	private AudioClip closeClip = null;
	[SerializeField]	private AudioClip dingClip = null;
	private AudioSource audioSource = null;
	private bool inElevator = false;

	// Use this for initialization
	void Start () {
		audioSource = GetComponent<AudioSource>();
	}

	IEnumerator DingDelay()
	{
		yield return new WaitForSeconds(1.0f);
		audioSource.PlayOneShot(dingClip);
	}

	IEnumerator GameComplete()
	{
		SCR_Player.DisableInput = true;

		yield return new WaitForSeconds(1.0f);

		SceneManager.LoadScene("Victory");
	}

	// Update is called once per frame
	void Update () {
		if (inElevator) {
			StartCoroutine ("ElevatorDelay");

			// This needs to be here, else the coroutine would repeat.
			// Therefore the elevator would shoot up multiple floors.
			inElevator = false;
		}

		if (SCR_GameVariables.currentPlugCount == SCR_GameVariables.roomPlugCount) {

			if(SCR_GameVariables.currentPlugCount == 4)
			{
				//Debug.Log("Completed room 3");
				//SceneManager.LoadScene("Victory");
				StartCoroutine("GameComplete");
			}
			else
			{
				SCR_GameVariables.roomPlugCount *= 2;
				SCR_GameVariables.currentPlugCount = 0;
				transform.FindChild ("ElevatorDoor_Right").GetComponent<SCR_OpenDoor> ().OpenElevator (true);
				transform.FindChild ("ElevatorDoor_Left").GetComponent<SCR_OpenDoor> ().OpenElevator (true);
				audioSource.PlayOneShot(openClip);
				StartCoroutine("DingDelay");
			}
		}
	}

	IEnumerator ElevatorDelay()
	{
		yield return new WaitForSeconds (2.0f);
		GetComponent<SCR_Elevator> ().TriggerElevator ();
	}

	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.tag == "Player") {
			inElevator = true;
			transform.FindChild ("ElevatorDoor_Right").GetComponent<SCR_OpenDoor> ().OpenElevator (false);
			transform.FindChild ("ElevatorDoor_Left").GetComponent<SCR_OpenDoor> ().OpenElevator (false);
			audioSource.PlayOneShot(closeClip);
		}
	}
	void OnTriggerExit(Collider col)
	{
		if (col.gameObject.tag == "Player") {
			inElevator = false;
			transform.FindChild ("ElevatorDoor_Right").GetComponent<SCR_OpenDoor> ().OpenElevator (false);
			transform.FindChild ("ElevatorDoor_Left").GetComponent<SCR_OpenDoor> ().OpenElevator (false);
		}
	}

	public AudioClip OpenClip
	{
		get { return openClip; }
	}

	public AudioClip CloseClip
	{
		get { return closeClip; }
	}

	public AudioClip DingClip
	{
		get { return dingClip; }
	}
}
