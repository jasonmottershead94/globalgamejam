﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.CrossPlatformInput;

public class SCR_Footsteps : MonoBehaviour {

	[SerializeField]List<AudioClip> footsteps = new List<AudioClip>();
	private AudioSource audioSource = null;

	// Use this for initialization
	void Start () {
		audioSource = gameObject.GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {}

	public void Play()
	{
		if (footsteps.Count > 0) {
			int rand = Random.Range (0, footsteps.Count);
			audioSource.clip = footsteps[rand];
			audioSource.Play();
		}
	}
}
