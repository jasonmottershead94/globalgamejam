﻿using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
using System.Collections;

public class SCR_Camera : MonoBehaviour {

	[SerializeField]	private Vector2 movementSpeed = new Vector2(1.0f, 1.0f);
	[SerializeField]	private float cameraRotationYLimit = 40.0f;
	[SerializeField]	private bool clampVertical = true;
	private Transform cam;
	private Quaternion newRotate;
	private float rotationX = 0.0f;

	// Use this for initialization
	void Start () {
		Cursor.lockState = CursorLockMode.Locked;
		cam = transform.FindChild("Main Camera");
	}

	void Move()
	{
		if(CrossPlatformInputManager.GetAxis("Mouse X") > 0.25f || CrossPlatformInputManager.GetAxis("Mouse X") < -0.25f)
		{
			transform.Rotate(new Vector3(0.0f, CrossPlatformInputManager.GetAxis("Mouse X") * movementSpeed.y, 0.0f));
		}

		if(clampVertical)
		{
			rotationX += Input.GetAxis("Mouse Y") * -movementSpeed.x;

			rotationX = Mathf.Clamp (rotationX, -40, 40);

			cam.localEulerAngles = new Vector3 (rotationX, cam.transform.localEulerAngles.y, cam.transform.localEulerAngles.z);

		}

		if(Input.GetMouseButtonDown(1))
		{
			transform.rotation.eulerAngles.Set(0.0f, 0.0f, 0.0f);
			Camera.main.transform.eulerAngles.Set(0.0f, 0.0f, 0.0f);
			Camera.main.transform.forward = new Vector3(0.0f, 0.0f, 1.0f);
		}
	}

	// Update is called once per frame
	void Update () {
	 	Move();
	}
}
