﻿using UnityEngine;
using System.Collections;

public class SCR_Credits : MonoBehaviour
{

	private float timer;


	// Use this for initialization
	void Start ()
	{
		timer = 5.0f;
	}

	// Update is called once per frame
	void Update () 
	{
		timer -= Time.deltaTime;

		if (timer <= 0.0f || Input.GetKey(KeyCode.Space) || Input.GetMouseButton(0))
		{
			Application.Quit ();
		}
	}
}
