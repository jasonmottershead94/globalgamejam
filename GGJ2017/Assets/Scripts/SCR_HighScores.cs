﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

// High scores is just a standard game object.
// Here we will manage player high scores.
public class SCR_HighScores : MonoBehaviour 
{

	// Attributes.
	private static int highestScore = 0;												// This will store the current highest score in the leaderboard.
	private static string highestScorer = "";
	private HighScore[] highScoresList;													// Access to our highscore struct.
	private const string privateCode = "ebSafvlouE-EhLsCd-OvqQ4UdjqmVTzky_z94zNh01Pw";	// Our private code for highscores, so that we have private access for uploads to the online scoreboard.
	private const string publicCode = "588424d0b6dd1500a4de3091";						// The public code for viewing scores, used for downloading the scores.
	private const string webURL = "http://dreamlo.com/lb/";								// The web URL for view scores.
	private const string addCode = "/add/";												// What we need to use in order to add to our scoreboard.
	private const string downloadCode = "/pipe/";										// What we need to use in order to download from our scoreboard.
	//private InputField usernameField = null;											// User to access the username of the player.
	//private GameObject usernameObject = null;											// User to bring down the username field with a background.
	//private Button menuButton = null, submitButton = null, restartButton = null;		// Used to enable/disable button use whilst entering a username.
	private SCR_DisplayHighScores highScoreDisplay = null;									// Used to pass along username and scores.
	private static SCR_HighScores highScoresInstance = null;								// Used to make static methods.

	// Methods.
	/**
	 * This function will be called before initialisation.
	 */
	void Awake()
	{
		if(highScoresInstance == null)
		{
			// Setting a static instance.
			highScoresInstance = this;
		}

		// Initialise the high score display.
		highScoreDisplay = GetComponent<SCR_DisplayHighScores>();

	}

	/**
	 * Here we will allow the user to enter a username for
	 * their score.
	 */
	public void SubmitButtonClicked()
	{
		// Add a new score to the leaderboard.
		// This will be for any score achieved.
		AddNewHighScore(SCR_GameVariables.Username, (int)SCR_GameVariables.timer);

		// Remove the submit button from the user so that they cannot just post the score again.
		//GameObject.Find("SubmitButton").transform.position = UIConstants.OFF_SCREEN;

	}

	/**
	 * If the user changes their mind and doesn't want
	 * to submit a high score.
	 */
	public void CancelButtonClicked()
	{

		// Move the username interface from the screen.
		//usernameObject.transform.position = UIConstants.OFF_SCREEN;

		// Set a different game object as selected.
		//EventSystem.current.SetSelectedGameObject(usernameObject);

	}

	/**
	 * Starts the coroutine that will upload a new score.
	 */
	public static void AddNewHighScore(string username, int score)
	{

		// This will start a timed delay to upload a high score.
		highScoresInstance.StartCoroutine(highScoresInstance.UploadNewHighScore(username, score));

	}

	/**
	 * This function will allow us to upload a new highscore,
	 * with a username and score.
	 */
	IEnumerator UploadNewHighScore(string username, int score)
	{

		// Add a score to our online leaderboard.
		WWW www = new WWW(webURL + privateCode + addCode + WWW.EscapeURL(username) + "/" + score);

		// Wait until the upload has completed.
		yield return www;

		// If we have successfully uploaded our score.
		if(string.IsNullOrEmpty(www.error)){

			// Success response.
			print("Upload Successful");

			// Download the new highscores.
			//DownloadHighScores();

		}
		// Otherwise, we haven't been able to upload our score.
		else {

			// Failure response.
			print("Error Uploading: " + www.error);

		}

	}

	/**
	 * Starts the coroutine that will download the scoreboard.
	 */
	public void DownloadHighScores()
	{

		// This will start a timed delay to download the high scores.
		StartCoroutine("DownloadHighScoresFromDatabase");

	}

	/**
	 * This function will allow us to download the highscores.
	 */
	IEnumerator DownloadHighScoresFromDatabase()
	{

		// Download our online leaderboard.
		WWW www = new WWW(webURL + publicCode + downloadCode);

		// Wait until the download has completed.
		yield return www;

		// If we have successfully downloaded our scoreboard.
		if(string.IsNullOrEmpty(www.error))
		{
			// Success response.
			// Format the high scores to have the username and score.
			FormatHighScores(www.text);

			//if(SceneManager.GetActiveScene() == SceneManager.GetSceneByName(SCR_SceneNames.HIGH_SCORE_SCENE))
			//{
				// Update the high score display once they have downloaded.
				highScoreDisplay.OnHighScoresDownloaded(highScoresList);
			//}
			//else
			//{
				GetLatestEntry();
			//}

		}
		// Otherwise, we haven't been able to download our scoreboard.
		else 
		{

			// Failure response.
			print("Error Downloading: " + www.error);

		}

		// Wait for 1 second.
		yield return new WaitForSeconds(1.0f);

	}

	/**
	 * Here we will access the highest score on the leaderboard.
	 */
	public void GetLatestEntry()
	{
		if(highScoresList.Length > 0)
		{
			// Return the latest leaderboard score.
			highestScore = highScoresList[0].score;
			SCR_GameVariables.HighestScore = highestScore;

			highestScorer = highScoresList[0].username;
		SCR_GameVariables.HighestScorerUsername = highestScorer;
		}
	}

	/**
	 * Here we will format the high scores so we can obtain the username and score.
	 */
	void FormatHighScores(string textStream)
	{

		// This will format our scores using the new lines, and removes any empty lines added at the bottom.
		string[] entries = textStream.Split(new char[] {'\n'}, System.StringSplitOptions.RemoveEmptyEntries);

		// Initialising our high scores list to be the correct length.
		highScoresList = new HighScore[entries.Length];

		// Loop through our text stream.
		for(int i = 0; i < entries.Length; i++)
		{

			// Split up the text obtained from the scoreboard to remove the pipe symbols.
			string[] entryInfo = entries[i].Split(new char[]{'|'});

			// Obtain the username and scores.
			string username = entryInfo[0];
			int score = int.Parse(entryInfo[1]);

			// Fill up the highscores list.
			highScoresList[i] = new HighScore(username, score);

		}

	}

	public static int HighestScore
	{
		get { return highestScore; }
		set { highestScore = value; }
	}

	public static string HighestScorer
	{
		get { return highestScorer; }
		set { highestScorer = value; }
	}

	public HighScore[] HighScoresList
	{
		get { return highScoresList; }
		//set { highScoresList = value; }
	}

}

/**
 * This will be used to store player data for high scores.
 */
public struct HighScore
{

	// Attributes.
	public string username;	// The username of the player.
	public int score;		// The high score the player achieved.

	// Methods.
	/**
	 * This will be used to access the player names and high scores from the list.
	 */
	public HighScore(string playerName, int playerScore)
	{

		// Initialising our attributes.
		username = playerName;
		score = playerScore;

	}

}