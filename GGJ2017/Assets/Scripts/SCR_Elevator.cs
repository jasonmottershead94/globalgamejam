﻿using UnityEngine;
using System.Collections;

public class SCR_Elevator : MonoBehaviour {

	[SerializeField] private float speed = 1.0f;
	[SerializeField]	private AudioClip audioClip = null;
	private bool trigger = false;
	private const float floorGap = 2.9f;
	private float[] elevatorStops = new float[]{floorGap, floorGap * 3.0f};
	private Transform player;
	private AudioSource audioSource = null;
	private SCR_ElevatorDoorTigger doorTrigger = null;

	// Use this for initialization
	void Start () 
	{
		player = GameObject.FindWithTag ("Player").transform;
		audioSource = GetComponent<AudioSource>();
		doorTrigger = GetComponent<SCR_ElevatorDoorTigger>();
	}

	IEnumerator OpenDelay()
	{
		yield return new WaitForSeconds(1.0f);

		audioSource.PlayOneShot(doorTrigger.OpenClip);
	}

	void StopMoving()
	{
		SCR_GameVariables.currentFloor++;
		SCR_GameVariables.currentWaterLevel = 0.0f;
		player.SetParent (null);
		transform.FindChild ("ElevatorDoor_Right").GetComponent<SCR_OpenDoor> ().OpenElevator (true);
		transform.FindChild ("ElevatorDoor_Left").GetComponent<SCR_OpenDoor> ().OpenElevator (true);
		trigger = false;
		GameObject.Find("PRE_Water").transform.SetParent(null);
		GameObject.Find("PRE_Water").transform.Translate(new Vector3(0.0f, -1.0f, 0.0f));
		//audioSource.PlayOneShot(doorTrigger.OpenClip);
		StartCoroutine("OpenDelay");
		Debug.Log("Should stop elevator.");
	}

	// Update is called once per frame
	void Update () {
		if (trigger) {
			transform.position = new Vector3 (transform.position.x, transform.position.y + (speed * 0.5f), transform.position.z);
			GameObject.Find("PRE_Water").transform.SetParent(transform);
			if (transform.position.y > elevatorStops [SCR_GameVariables.currentFloor])
			{
				StopMoving();
			}
		}


		//if (Input.GetKeyDown (KeyCode.P)) {
		//	TriggerElevator ();
		//}

		//if(Input.GetKeyDown(KeyCode.B))
		//{
		//	GetComponent<AudioSource>().PlayOneShot(audioClip);
		//}
	}

	public void TriggerElevator()
	{
		trigger = true;
		player.SetParent (transform);
	}
}
