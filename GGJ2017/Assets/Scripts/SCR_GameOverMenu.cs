﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SCR_GameOverMenu : MonoBehaviour 
{

	// Use this for initialization
	void Start () 
	{
		Cursor.lockState = CursorLockMode.None;
		SCR_HighScores.AddNewHighScore("Jason", (int)SCR_GameVariables.timer);
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void RetryPressed ()
	{
		SCR_GameVariables.Reset();
		SCR_Player.DisableInput = true;
		SceneManager.LoadScene (2);
	}

	public void QuitPressed ()
	{
		SceneManager.LoadScene (6);
	}
}
