﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class SCR_Water : MonoBehaviour {

	[SerializeField] private float speed = 1.0f;
	[SerializeField]	private bool ignoreDeath = false;
	private SCR_Player player = null;

	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag("Player").GetComponent<SCR_Player>();
	}

	// Update is called once per frame
	void Update () {
		if(!SCR_Player.DisableInput)
		{
			transform.position = new Vector3 (transform.position.x, transform.position.y + (speed * 0.05f), transform.position.z);
			SCR_GameVariables.currentWaterLevel = transform.position.y;

			if(!ignoreDeath)
			{
				if ((player.transform.position.y - SCR_GameVariables.currentWaterLevel) < (SCR_GameVariables.waterKillLevel)) 
				{			
					//End game
					SceneManager.LoadScene("SCN_GameOver");
				}
			}
		}
	}

	public float Speed
	{
		get { return speed; }
		set { speed = value; }
	}
}
