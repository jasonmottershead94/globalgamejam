﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SCR_Hole : MonoBehaviour {

	// Attributes.
	[SerializeField]	private GameObject plug = null;
	[SerializeField]	private Text holeText = null;
	[SerializeField]	private float decreaseRate = 0.8f;
	[SerializeField]	private AudioClip fillAudio = null;
	private AudioSource audioSource = null;
	private bool inTrigger = false;
	private SCR_Player player = null;
	private Transform hole;
	private Vector3 onScreen = Vector3.zero;
	private Vector3 offScreen = new Vector3(0.0f, 3000.0f, 0.0f);
	private bool isFilled = false;
	private SCR_Water water = null;

	// Use this for initialization
	void Start ()
	{
		player = GameObject.FindGameObjectWithTag("Player").GetComponent<SCR_Player>();
		hole = transform.parent.FindChild("HoleTransform");
		onScreen = holeText.transform.position;
		holeText.transform.position = offScreen;
		water = GameObject.Find("PRE_Water").GetComponent<SCR_Water>();
		audioSource = GetComponent<AudioSource>();

		Text[] text = GameObject.Find("HolePlacementText").GetComponentsInChildren<Text>();

		for(int i = 0; i < text.Length; i++)
		{
			text[i].transform.position = offScreen;			
		}
	}

	void OnTriggerEnter(Collider coll)
	{
		if(coll.tag == "Player")
		{
			inTrigger = true;
			player.Interacting = true;
		}
	}

	void OnTriggerExit(Collider coll)
	{
		if(coll.tag == "Player")
		{
			inTrigger = false;
			player.Interacting = false;
		}
	}

	void CorrectItem()
	{
		audioSource.Play();
		player.PickupItem.collider.tag = "Untagged";
		player.Carrying = false;
		inTrigger = false;
		isFilled = true;
		player.PickupItem.collider.transform.position = hole.position;
		player.PickupItem.collider.transform.rotation = Quaternion.Euler(hole.rotation.eulerAngles.x, hole.rotation.eulerAngles.y, hole.rotation.eulerAngles.z);
		player.PickupItem.collider.transform.SetParent(hole);
		player.PickupItem.collider.GetComponent<Rigidbody>().isKinematic = true;
		SCR_GameVariables.currentPlugCount++;
		SCR_GameVariables.overallPlugCount++;
		water.Speed *= decreaseRate;
	}

	void CheckItem()
	{
		// If the player has picked up an object AND if the picked up item is the correct plug.
		if(player.PickupItem.collider.gameObject != null && player.PickupItem.collider.gameObject == plug)
		{
			if(player.PickupItem.collider.tag == "Pickup")
				CorrectItem();
		}
		else if(player.PickupItem.collider.gameObject != plug)
			player.Pickup(player.PickupItem);
	}

	// Update is called once per frame
	void Update () 
	{
		if(inTrigger && !isFilled)
		{
			holeText.transform.position = onScreen;

			if(Input.GetMouseButtonDown(0))
				CheckItem();
		}
		else
		{
			holeText.transform.position = offScreen;
		}

		if(!player.Carrying)
			holeText.transform.position = offScreen;
	}
}