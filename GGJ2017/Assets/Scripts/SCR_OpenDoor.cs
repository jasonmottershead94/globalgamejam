﻿using UnityEngine;
using System.Collections;

public class SCR_OpenDoor : MonoBehaviour {

	[SerializeField]private float speed = 0.1f;
	private bool isMoving = false;
	private float direction = 0.0f;
	private float increment = 0.0f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (isMoving) {
			transform.Translate (speed * direction, 0.0f, 0.0f);
			increment += speed;
			if (increment > 2.0f) {
				isMoving = false;
				increment = 0.0f;
			}
		}

		//if (Input.GetKeyDown (KeyCode.O)) 
		//{
		//	OpenElevator (true);
		//}

		//if (Input.GetKeyDown (KeyCode.I)) 
		//{
		//	OpenElevator (false);
		//}
	}

	public void OpenElevator(bool flag)
	{
		isMoving = true;
		if (flag) {
			direction = 1.0f;
		} 
		else {
			direction = -1.0f;
		}
	}
}