﻿using UnityEngine;
using System.Collections;

public class SCR_GameVariables {
	public static int currentFloor = 0;
	public static float currentWaterLevel = 0.0f;
	public static float waterKillLevel = 1.5f;
	public static float timer = 0.0f;
	public static int currentPlugCount = 0;
	public static int overallPlugCount = 0;
	public static int roomPlugCount = 1;
	public static string NullUsername = "null_username";
	public static string Username = NullUsername;
	public static int HighestScore = 0;
	public static string HighestScorerUsername = NullUsername;

	public static void Reset()
	{
		currentFloor = 0;
		currentWaterLevel = 0.0f;
		timer = 0.0f;
		currentPlugCount = 0;
		overallPlugCount = 0;
		roomPlugCount = 1;
	}
}