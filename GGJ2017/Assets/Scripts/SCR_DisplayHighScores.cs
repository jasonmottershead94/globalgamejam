﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

/**
 * Display high scores is just a standard game object.
 * With this class we will display high scores.
 */
public class SCR_DisplayHighScores : MonoBehaviour 
{

	// Attributes.
	private List<Text> highScoreUsernameText;
	private List<Text> highScoreText;					// Access to the list of high scores.
	private SCR_HighScores highScoreManager;			// Used to access our high scores from the database.
	private float refreshRate = 30.0f;					// How often our highscores will download and update the highscores.
	private int yCellPosition = -100;					// How far down the next cell for the text should be.
	private GameObject content = null;					// Accessing the high scores content box.
	private Scrollbar scrollBar = null;
	private List<string> usernames = null;

	// Methods.
	/**
	 * This function will be called for initialisation.
	 */
	void Start ()
	{

		// Initialise our attributes.
		highScoreUsernameText = new List<Text>();
		highScoreText = new List<Text>();
		usernames = new List<string>();
		highScoreManager = GetComponent<SCR_HighScores>();

		if(SceneManager.GetActiveScene() == SceneManager.GetSceneByName("SCN_HighScores"))
		{
			content = GameObject.Find("Content");
			scrollBar = GameObject.Find("Scrollbar Vertical").GetComponent<Scrollbar>();
		}

		//InternetConnection.CheckNetworkConnection();

		// Download the high scores board.
		highScoreManager.DownloadHighScores();

		// If we want the high scores to update constantly, then we can use this line.
		// Although, I believe at the moment it will create empty entries instead of dupliate ones.
		// Start high score updates.
		//StartCoroutine("RefreshHighScores");
	}

	/**
	 * This function will delay a frame to set the position of the scroll bar.
	 */
	IEnumerator ScrollbarDelay()
	{

		// Wait for a frame.
		yield return new WaitForSeconds(0.1f);

		// Set the position of the scroll bar.
		scrollBar.value = 1.0f;

	}

	void SetIndividualLeaderboardText(HighScore[] highScoreList, List<Text> leaderboardTextElements, int loopIndex, string additiveMessage)
	{
		if(SceneManager.GetActiveScene() == SceneManager.GetSceneByName("SCN_HighScores"))
		{
			if(leaderboardTextElements == highScoreUsernameText)
			{
				leaderboardTextElements[loopIndex].text = loopIndex + 1 + ".";
			}
			else
			{
				leaderboardTextElements[loopIndex].text = "";
			}
		}

		if(highScoreList.Length > loopIndex)
		{
			if(SceneManager.GetActiveScene() == SceneManager.GetSceneByName("SCN_HighScores"))
			{
				leaderboardTextElements[loopIndex].text += additiveMessage;
			}

			//if(usernames.Count == 0)
			//{
			//	usernames.Add(highScoreList[loopIndex].username);
			//}
		}
	}

	void SetLeaderboardText(HighScore[] highScoreList, List<Text> leaderboardTextElements, string additiveMessage)
	{
		for(int i = 0; i < leaderboardTextElements.Count; i++)
		{
			SetIndividualLeaderboardText(highScoreList, leaderboardTextElements, i, additiveMessage);
		}
	}

	IEnumerator DownloadDelay(HighScore[] highScoreList)
	{

		SetLeaderboardText(highScoreList, highScoreUsernameText, "Fetching...");
		SetLeaderboardText(highScoreList, highScoreText, "Fetching...");

		yield return new WaitForSeconds(1.0f);

		for(int i = 0; i < highScoreUsernameText.Count; i++)
		{
			SetIndividualLeaderboardText(highScoreList, highScoreUsernameText, i, highScoreList[i].username);
		}

		for(int i = 0; i < highScoreText.Count; i++)
		{
			SetIndividualLeaderboardText(highScoreList, highScoreText, i, highScoreList[i].score.ToString());
		}

	}

	/**
	 * This will change the text values with the values
	 * from our high scores.
	 */
	public void OnHighScoresDownloaded(HighScore[] highScoreList)
	{
		// If we have a set of high scores.
		if(highScoreManager.HighScoresList != null)
		{
			if(SceneManager.GetActiveScene() == SceneManager.GetSceneByName("SCN_HighScores"))
			{
				// Set the current y value for the top score.
				int previousY = 1;

				// Loop through each high score.
				for(int i = 0; i < highScoreManager.HighScoresList.Length; i++)
				{
					// Calculate the next position for the score.
					Vector2 tempNextPosition = new Vector2(0.0f, (previousY + ((int)yCellPosition * i)));

					// Create a new instance of our score text.
					GameObject tempHighScoreText = Instantiate(Resources.Load("Prefabs/PRE_Score", typeof (GameObject)), tempNextPosition, Quaternion.identity) as GameObject;
					tempHighScoreText.transform.SetParent(content.transform, false);

					// Add the score text into our array list for processing.
					highScoreUsernameText.Add(tempHighScoreText.transform.FindChild("Username").GetComponent<Text>());
					highScoreText.Add(tempHighScoreText.transform.FindChild("Score").GetComponent<Text>());
				}
			}
		}

		StartCoroutine(DownloadDelay(highScoreList));

		for(int i = 0; i < highScoreList.Length; i++)
		{
			usernames.Add(highScoreList[i].username);
		}

		if(SceneManager.GetActiveScene() == SceneManager.GetSceneByName("SCN_HighScores"))
		{
			// Start the scroll bar delay.
			// This will set the scroll bar to the correct position.
			StartCoroutine("ScrollbarDelay");
		}
	}

	/**
	 * This function will just update our highscores every X seconds.
	 */
	IEnumerator RefreshHighScores()
	{
		// Constantly update.
		while(true)
		{
			// Download the high scores board.
			highScoreManager.DownloadHighScores();

			// Wait for X seconds before downloading scores again.
			yield return new WaitForSeconds(refreshRate);
		}
	}

	public List<string> Usernames
	{
		get { return usernames; }
	}
}