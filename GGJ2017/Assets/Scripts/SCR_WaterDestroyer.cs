﻿using UnityEngine;
using System.Collections;

public class SCR_WaterDestroyer : MonoBehaviour {

	// Use this for initialization
	void Start () 
	{}

	void OnTriggerEnter(Collider coll)
	{
		if(coll.gameObject.tag != "Player" && coll.gameObject.tag != "Untagged")
		{
			Debug.Log("DESTROY");
			Destroy(coll.gameObject);
		}
	}

	// Update is called once per frame
	void Update () {
	
	}
}
