﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class SCR_Victory : MonoBehaviour {

	private Text timerText = null;

	// Use this for initialization
	void Start () 
	{
		Cursor.lockState = CursorLockMode.None;
		SCR_HighScores.AddNewHighScore("Jason", (int)SCR_GameVariables.timer);
		timerText = GameObject.Find("TimerText").GetComponent<Text>();
		timerText.text = "Time: " + SCR_GameVariables.timer;
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	public void RetryPressed ()
	{
		SCR_GameVariables.Reset();
		SCR_Player.DisableInput = true;
		SceneManager.LoadScene (2);
	}

	public void QuitPressed ()
	{
		SceneManager.LoadScene (6);
	}
}
